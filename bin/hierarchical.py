import sys
import os.path

import pandas as pd
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from sklearn.metrics import silhouette_score, silhouette_samples
from scipy.spatial.distance import pdist
from sklearn.metrics.cluster import adjusted_rand_score
from scipy.cluster.hierarchy import fcluster, linkage, dendrogram
from scipy.spatial.distance import squareform

## Argument parsing a input file reading

# Argument parsing
sim_file=sys.argv[1] # the output file from tomtom
outdir=sys.argv[2] # output directory
class_annotations=sys.argv[3] # file with the class annotations from JASPAR

# Read the input files
sim = pd.read_csv(sim_file, header=None, delimiter="\t", skiprows=1)
annots = pd.read_csv(class_annotations, header = 0, delimiter = "\t")

# Trnasform tomtom input file into square matrix
simsq = sim.pivot(index = 0, columns = 1, values = 4)
simsq.fillna(100, inplace=True)
mat = -np.log10(simsq)
mat[np.isinf(mat)]=10

# Compute linkage
Z = linkage(mat, method = 'complete', metric = 'correlation')

# Build labels, legend and colors for the dendrogram
rowids = simsq.index
labels = []
colors = []

for TF in rowids:
	TF_i = TF.split("_")[-1]
	labels.append(TF_i)
	col = annots[annots["NAME"] == TF_i]
	if col.shape[0] == 0:
		col = [0, 0, 0]
	else:
		col = col.iloc[[-1,]]
		col = col.values[0][-1].split(",")
		col = [float(item) for item in col]
		col = tuple(col)
	colors.append(col)

cols_dict = {}
for key in labels:
	for val in colors:
		cols_dict[key] = val
		colors.remove(val)
		break

legend_elements = []
classes = list(annots["VAL"])
classes = set(classes)

for class_i in classes:
	class_col = annots[annots["VAL"] == class_i].iloc[[0,]]
	class_col = class_col.values[0][-1].split(",")
	class_col = [float(item) for item in class_col]
	class_col = tuple(class_col)
	legend_elements.append(Patch(facecolor = class_col, edgecolor = 'black', label = class_i))

classes_ids = {}
i = 1
for key in classes:
	classes_ids[key] = i
	i = i + 1

if "Unknown" not in classes_ids:
	classes_ids["Unknown"] = len(classes_ids) + 1

# Store DBD class annotations per TF to later compute the adjusted rand score
real_labels = []

for matrix in rowids:
	mat_i = matrix.split("_")[0]
	mat_i = mat_i.split(".")[0]
	annot_i = annots[annots["BASE_ID"] == mat_i]
	if annot_i.shape[0] == 0:
		real_labels.append(classes_ids["Unknown"])
	else:
		annot_i = annot_i.iloc[[-1,]]
		class_i = annot_i.values[0][-2]
		real_labels.append(classes_ids[class_i])

# Get range of distance values and loop over all of them with incremental steps of 0.01
dists = pdist(mat, metric = "correlation")
start = np.min(dists)
end = np.max(dists)
step = 0.01

thresholds=np.arange(start, end+step/100, step)

rand_scores = []

# Loop over all distance values to obtain clusters for each distance threshold cut
for thresh in thresholds:

	fig = plt.figure()
	fig.set_size_inches(40,5)
	fig.legend(handles=legend_elements, loc='best')

	dendrogram(Z, orientation = "top", labels = labels, distance_sort="descending", color_threshold = thresh)
	ax = plt.gca()
	xlbls = ax.get_xmajorticklabels()
	for lbl in xlbls:
		lbl.set_color(cols_dict[lbl.get_text()])

	plt.axhline(y=thresh, color='r', linestyle='--', alpha = 0.2)

	print("tree height: %0.2f" % (thresh))

	cl_i = fcluster(Z, thresh, criterion='distance')

	rand_scores.append(adjusted_rand_score(real_labels, list(cl_i)))

	df = pd.DataFrame(cl_i, index=mat.index, columns=["cluster"])
	df.index.name="motifs"

	df.to_csv(os.path.join(outdir, "clusters.%0.2f.txt" % (thresh)), sep="\t", header=True)

	plt.savefig(outdir + "/clusters." + str(round(thresh, 2)) + ".pdf")


# Plot distance threshold with best adjusted rand score based on JASPAR DBD annotations

fig = plt.figure()
fig.set_size_inches(40,5)
fig.legend(handles=legend_elements, loc='best')
dendrogram(Z, orientation = "top", labels = labels, distance_sort="descending", color_threshold = thresholds[np.argmax(rand_scores)])
ax = plt.gca()
xlbls = ax.get_xmajorticklabels()
for lbl in xlbls:
	lbl.set_color(cols_dict[lbl.get_text()])
plt.axhline(y=thresholds[np.argmax(rand_scores)], color='r', linestyle='--', alpha = 0.2)
cl = fcluster(Z, thresholds[np.argmax(rand_scores)], criterion='distance')
df = pd.DataFrame(cl, index=mat.index, columns=["cluster"])
df.index.name="motifs"

df.to_csv(os.path.join(outdir, "clusters_best_rand.%0.2f.txt" % (thresholds[np.argmax(rand_scores)])), sep="\t", header=True)

plt.savefig(outdir + "/clusters_best_rand." + str(round(thresholds[np.argmax(rand_scores)], 2)) + ".pdf")

# Plot adjusted Rand score distribution per distance threshold cut

x_pos = np.arange(len(thresholds))
sil_barplot = plt.figure()
plt.bar(x_pos, rand_scores)
plt.xticks(x_pos, [str(round(item, 2)) for item in thresholds])
plt.xticks(fontsize = 4, rotation = "vertical")
plt.xlabel('Distance threshold', fontsize = 10)
plt.ylabel('Adjusted Rand Score', fontsize = 10)
plt.savefig(outdir + "/adjusted_rand_scores.pdf")
##
