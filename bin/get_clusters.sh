#!/bin/bash

DIR=$( realpath "$( dirname "$0" )" )

JASPAR_dir=$1
output_dir=$2

orgs=$(find $JASPAR_dir -maxdepth 1 -mindepth 1 -type d)

for org in ${orgs[@]}
do

  mkdir -p $output_dir/$(basename $org)/tomtom/

  meme_file=$(find $org -name "aggregated_meme.meme")
  tomtom -dist kullback -text -min-overlap 1 -motif-pseudo 0.1 -thresh 1 $meme_file $meme_file > $output_dir/$(basename $org)/tomtom/tomtom.all.txt

  mkdir -p $output_dir/$(basename $org)/tomtom/clustering_results

  python $DIR/hierarchical.py $output_dir/$(basename $org)/tomtom/tomtom.all.txt $output_dir/$(basename $org)/tomtom/clustering_results $org/classes.csv

done
