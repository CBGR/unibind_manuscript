#!/bin/bash

DIR=$( realpath "$( dirname "$0" )" )

clusters_file=$1
tomtom_file=$2
meme_file=$3
output_dir=$4

JASPAR_dir=$(dirname $meme_file)

mkdir -p $output_dir/tomtom/processed_clusters

dname=$(basename $clusters_file)
dname=${dname//"clusters"/"height"}
dname=${dname//".txt"/""}

mkdir -p $output_dir/tomtom/processed_clusters/$dname

python $DIR/process_cluster.py $tomtom_file $clusters_file $output_dir/tomtom/processed_clusters/$dname/ $meme_file

mkdir -p $output_dir/tomtom/processed_clusters/$dname/plots

clusters=($(find $output_dir/tomtom/processed_clusters/$dname/ -name "cluster-info*.txt"))

for cl in ${clusters[@]}
do

  cl_motifs=${cl//"info"/"motifs"}
  outfile=$(basename $cl)
  outfile=${outfile//"cluster-info."/""}
  outfile=${outfile//".txt"/""}
  outfile=$output_dir/tomtom/processed_clusters/${dname}/plots/cluster${outfile}

  python $DIR/build_archetype_per_cluster.py $cl $cl_motifs $outfile $JASPAR_dir/
  python $DIR/viz_cluster.py $cl $cl_motifs $outfile $JASPAR_dir/

done
