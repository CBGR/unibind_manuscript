#!/bin/bash

DIR=$( realpath "$( dirname "$0" )" )

input_dir=$1
bed_file=$2
class_annots_file=$3
chroms_sizes=$4
output_dir=$5

cat $input_dir/cluster-info*.txt > $output_dir/concat_cluster-info.txt
cat $input_dir/cluster-motifs*.txt > $output_dir/concat_cluster-motifs.txt

# Make sure col4 has only MATID and that we have only 6 cols

python $DIR/reassign.py $output_dir/concat_cluster-info.txt $output_dir/concat_cluster-motifs.txt $bed_file $class_annots_file > $output_dir/reassigned_TFBSs.bed

cut -f 1-8 $output_dir/reassigned_TFBSs.bed | sort -k1,1 -k2,2n | uniq > $output_dir/reassigned_TFBSs.bed.tmp; mv $output_dir/reassigned_TFBSs.bed.tmp $output_dir/reassigned_TFBSs.bed

#sort -k1,1 -k2,2n $output_dir/reassigned_TFBSs.bed | rev | uniq -f 1 | rev > $output_dir/reassigned_TFBSs.bed.tmp; mv $output_dir/reassigned_TFBSs.bed.tmp $output_dir/reassigned_TFBSs.bed

Rscript $DIR/create_rgb_file.R $output_dir/reassigned_TFBSs.bed

printf 'table TFBSs_collapsed\n"Collapsed motifs matches according to archetypal TFBS"\n(\nstring  chrom;        "Reference sequence chromosome or scaffold"\nuint    chromStart;    "Start position of feature on chromosome"\nuint    chromEnd;    "End position of feature on chromosome"\nstring  name;        "Name of cluster"\nuint    score;        "Score"\nchar[1] strand;        "+ or - for strand"\nuint    thickStart;    "Coding region start"\nuint    thickEnd;    "Coding region end"\nuint    reserved;    "itemRgb"\n)\n' > $output_dir/tracks_archetypal_TFBSs.as

bigBed_file=$output_dir/reassigned_TFBSs.bed
bigBed_file=${bigBed_file//".bed"/".bb"}

bedToBigBed -as=$output_dir/tracks_archetypal_TFBSs.as -type=bed9 -tab $output_dir/reassigned_TFBSs.bed $chroms_sizes $bigBed_file
