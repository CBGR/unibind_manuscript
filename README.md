## Archetype computation

Archetypes for the TFBSs in UniBind were computed following the approach in [Vierstra et al. (2020)](https://doi.org/10.1038/s41586-020-2528-x) ([GitHub code](https://github.com/jvierstra/motif-clustering)). The scripts in this repository introduce some small changes that include porting the code to python3, a deeper exploration of the distance thresholding for cluster definition and the visualization of the archetypal motif for each cluster. 

## Dependencies

The pipeline contains bash, python and R scripts. The bash scripts are wrappers that will call the different python and R scripts for the processing steps. The software needed to run this pipeline is:

### Python

The pipeline uses python 3 (last tested version was python 3.6). The required python libraries are:

- Sys
- Os.path
- Re
- Pandas
- Numpy
- Scipy
- Matplotlib
- Sklearn
- Pylab

### R 

The last tested version is R 4.0.2.

### Tomtom

The pipeline uses the [tomtom](https://meme-suite.org/meme/tools/tomtom) tool from the MEME suite.

### bedToBigBed

The pipline also uses the [bedToBigBed](http://hgdownload.soe.ucsc.edu/admin/exe/) tool from UCSC to build bigBed files from the reassigned binding sites. 

## Running

### 1. Extract clusters at different distance thresholds

To obtain the archetype binding sites, start by running the `bin/get_clusters.sh` script. This script takes as input the path to the JASPAR directory in `data/JASPAR_by_taxon/` and a path to an output directory. 

```
bash bin/get_clusters.sh data/JASPAR_by_taxon/ results/
```

This step will compute all pairwise comparisons between matrices from a JASPAR taxon and find the clusters at various distance thresholds. The resulting trees and clusters will be saved as PDF and txt files at `output_dir/taxon/tomtom/clustering_results/`. This step will also compute the adjusted Rand score for each distance threshold based on the DBD annotations from the JASPAR class annotations in `data/JASPAR_by_taxon/taxon/classes.csv`. The adjusted Rand score will also be used to find the optimal distance threshold.

### 2. Select a distance threshold of interest

Based on the results from step 1, select a distance threshold. The thresholds used for the manuscript were:

- Fungi: 0.81
- Insects: 0.90
- Nematodes: 1.01
- Plants: 0.92
- Vertebrates: 0.81

### 3. Process clusters at selected distance threshold

Once having selected a distance threshold, run the following script:

```
bash bin/process_selected_clusters.sh path/to/selected/clusters/file.txt path/to/tomtom/file path/to/meme/file output_directory
```

where the arguments are:

- path/to/selected/clusters/file.txt: path to the txt file with the clusters obtained at the desired distance threshold. This is located in the `output_dir/clustering_results` directory from step 1. It should have a form similar to `results/taxon/tomtom/clustering_results/clusters.*.txt`, where * is the selected distance threshold.
- path/to/tomtom/file: path to the tomtom output with the pairwise comparisons. This is located in the results directory from step 1. It should have a form like `results/tomtom/tomtom.all.txt`.
- path/to/meme/file: path to the file with all the JASPAR motifs for a taxon of interest. It is located at `data/JASPAR_by_taxon/taxon/aggregated_meme.meme`, where taxon is the taxon of interest.

This step will produce several files for each cluster, including the archetype logos and files with all the archetype members aligned and the archetypal motif boundaries.

### 4. Process the BED files with the new archetypes

This step will take the archetypes computed at the selected distance threshold and process a BED file with TFBSs to reassign each TFBS to its archetype.

```
bash bin/process_tracks.sh input/dir bed/file class/annotations chrom/sizes output/dir
``` 

where the arguments are:

- input/dir: the directory containing the txt files with the results of step 3. It should have a form similar to `results/taxon/tomtom/processed_clusters/height.*/`, where * is the selected distance threshold in step 2.
- bed/file: path to the input BED-like file with the TFBSs to reassign. This file should contain 7 tab-separated columns with the following information: chromosome, start, end, JASPAR matrix ID (e.g. MA0007.3), score, strand, TF name. 
- class/annotations: path to the class annotations file in `data/JASPAR_by_taxon/taxon/classes.csv`, where taxon is the taxon of interest.
- chrom/sizes: path to a chromosome sizes file for the taxon of interest. 
- output/dir: path to the output directory where the reassigned tracks will be saved.

This script will output the reassigned BED files in bigBed format.
